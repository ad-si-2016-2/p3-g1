# Um pouco sobre o WebSocket


[![N|Solid](https://dret.net/lectures/iot-spring15/img/websockets-logo.png)](https://www.html5rocks.com/pt/tutorials/websockets/basics/)


### Websocket na tratativa do problema: conexões de baixa latência de cliente-servidor e servidor-cliente


A web tem sido construída com base no conhecido paradigma de solicitação/resposta de HTTP. Um cliente carrega uma página da web e, em seguida, nada acontece até que o usuário clique na próxima página. Por volta de 2005, o AJAX começou a deixar a web mais dinâmica. Mesmo assim, toda a comunicação HTTP era direcionada pelo cliente, o que exigia interação do usuário ou sondagem periódica para carregar novos dados do servidor.

As tecnologias que permitem que o servidor envie dados ao cliente no mesmo momento em que constata que novos dados estão disponíveis foram usadas por algum tempo. Elas eram conhecidas por nomes como "Push" ou "Comet". Um dos problemas mais comuns para criar a ilusão de uma conexão iniciada pelo servidor é a chamada sondagem longa. Com a sondagem longa, o cliente abre uma conexão HTTP com o servidor que permanece aberta até que a resposta seja enviada. Sempre que tem novos dados, o servidor envia a resposta. A sondagem longa e as outras técnicas funcionam muito bem. Você as utiliza todos os dias em aplicativos como o chat do Gmail. No entanto, todas essas soluções compartilham um problema: elas carregam a sobrecarga de HTTP, que não é adequada para aplicativos de baixa latência. Um bom exemplo são jogos com vários jogadores no navegador ou em qualquer outro jogo on-line com um componente em tempo real.

Para reslver tal situtação, o WebSocket trouxe sockets para a web. A especificação WebSocket define uma API que estabelece conexões de socket entre um navegador da web e um servidor. Em outras palavras, há uma conexão persistente entre o cliente e o servidor e ambas as partes podem começar a enviar dados a qualquer momento.
A alternativa usada ao padrão para conexão http em websocket é o "ws" e "wss" para conexões seguras, assim como o https.

Sendo um protocolo moderno, a comunicação de origem cruzada está integrada diretamente no WebSocket. Embora ainda deva se comunicar somente com clientes e servidores confiáveis, o WebSocket permite a comunicação entre partes de qualquer domínio. O servidor decide se disponibilizará seu serviço para todos os clientes ou somente para os que residem em um conjunto de domínios bem definidos.



### Servidores proxy
  Cada nova tecnologia tem um novo conjunto de problemas. No caso do WebSocket, é a compatibilidade com os servidores proxy que faz a mediação das conexões HTTP na maioria das redes corporativas. O protocolo WebSocket usa o sistema de upgrade HTTP (que normalmente é usado para HTTP/SSL) para fazer "upgrade" de uma conexão HTTP para uma conexão WebSocket. Alguns servidores proxy não gostam disso e abandonarão a conexão. Assim, mesmo se um determinado cliente usar o protocolo WebSocket, talvez não seja possível estabelecer uma conexão.



### Casos de uso 
O WebSocket é interessante sempre que precisar de uma conexão quase em tempo real de baixa latência entre o cliente e o servidor. Isso pode envolver a reformulação do modo como se cria os aplicativos de servidor com um novo foco em tecnologias como filas de eventos. Alguns exemplos de casos de uso:

- Jogos on-line de vários jogadores
- Aplicativos de chat
- Links para esportes ao vivo
- Atualização em tempo real de redes sociais
  

--
*Adaptado de texto de Malte Ubl e Eiji Kitamura*