# Micro IRC WebSocket

[![N|Solid](http://cdn.socket.io/website/imgs/logo.svg)](http://socket.io/)

Uma simples implementação do protocolo IRC com WebSocket em Node.js para a matéria de Aplicações Distribuidas no curso de Sistemas de Informação da Universidade Federal de Goiás.

### Tecnologias Utilizadas
  - Node.js
  - WebSocket
  - Jquery