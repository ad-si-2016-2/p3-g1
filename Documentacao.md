# Documentação

[![N|Solid](http://cdn.socket.io/website/imgs/logo.svg)](http://socket.io/)

### Alguns detalhes sobre nosso projeto:

- No desenvolvimento desse software foi utilizado o protocolo de comunicação WebSocket. 
- Foi decidido utilizar tecnologias JavaScript. Optou-se por usar o pacote socket.io. 
- WebSocket fornece comunicação full-duplex entre cliente e servidor, sobre o protocolo TCP. 
- Nessa aplicação o pacote socket.io realiza a comunicação entre o cliente e o servidor da mesma. 
- A comunicação se estabelece através da função socket.on(), utilizada tanto no lado cliente quanto no servidor. 
- O comando socket.send() é usado para emitir eventos os quais serão comandos do protocolo IRC.

### Tecnologias Utilizadas
  - Node.js
  - WebSocket
  - Jquery
  

### Aviso 
Para utilizar esse projeto é necessário instalar o Socket.io para Node.js. 
Como? Se estiver usando Linux basta executar o seguinte comando dentro da pasta do projeto:
- $ sudo npm install socket.io

### Introdução  
Os WebSockets têm por finalidade proporcionar uma ligação persistente entre um cliente e um servidor onde ambas as partes podem ser utilizadas para iniciar o envio de dados a qualquer momento. Desta forma, o cliente estabelece uma conexão WebSocket através de um processo, no qual o cliente envia um pedido via HTTP para o servidor de forma regular, com isso, um cabeçalho de atualização vai incluso neste pedido que informa ao servidor que o cliente pretende estabelecer uma conexão via WebSocket.

### Servidor
O uso de WebSocket cria um padrão de uso totalmente novo para aplicativos de servidor. Embora pilhas de servidor tradicionais como LAMP sejam desenvolvidas com base no ciclo de solicitação/resposta HTTP, elas geralmente não lidam bem com um grande número de conexões WebSocket abertas. Manter um grande número de conexões abertas ao mesmo tempo requer uma arquitetura que receba alta concorrência com baixo desempenho. Essas arquiteturas normalmente são desenvolvidas com base em encadeamento ou no chamado IO sem bloqueio.

### Proxy
Cada nova tecnologia tem um novo conjunto de problemas. No caso do WebSocket, é a compatibilidade com os servidores proxy que faz a mediação das conexões HTTP na maioria das redes corporativas. O protocolo WebSocket usa o sistema de upgrade HTTP (que normalmente é usado para HTTP/SSL) para fazer "upgrade" de uma conexão HTTP para uma conexão WebSocket. Alguns servidores proxy não gostam disso e abandonarão a conexão. Assim, mesmo se um determinado cliente usar o protocolo WebSocket, talvez não seja possível estabelecer uma conexão.

### Protocolo
O protocolo com fio (um handshake e a transferência de dados entre cliente e servidor) para WebSocket é o RFC6455. As últimas versões do Google Chrome e do Google Chrome para Android são totalmente compatíveis com RFC6455, incluindo mensagens binárias. Além disso, o Firefox e o Internet Explorer são compatíveis nas versões mais recentes. As versões mais antigas do protocolo podem ser utilizadas, mas isso não é recomendado porque sabe-se que elas são vulneráveis.

### Referências
https://tools.ietf.org/html/draft-ietf-hybi-thewebsocketprotocol-03

http://socket.io/

https://www.w3.org/TR/websockets/

https://www.websocket.org/

