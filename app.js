var net = require("net");
var app = require('http').createServer(handler)
, io = require('socket.io').listen(app)
, fs = require('fs')


var port = 5000;

app.listen(port);

function handler (req, res) {
	fs.readFile('public/html/clienteweb.html', function (err, data) {
		if(err) {
			res.setHeader( 'Content-Type', 'text/html;charset=UTF-8' );
			res.writeHead(500);
			return res.end('<h3>Não foi possível carregar o html!<br />Verifique se o arquivo está dentro da pasta public/html/</h3>');
			}
		res.setHeader( 'Content-Type', 'text/html;charset=UTF-8' );
		res.writeHead(200);
		res.end(data);
	});
}


io.sockets.on('connection', function (webSocket) {
	var tcpStream = new net.Stream;
	tcpStream.setTimeout(0);
	tcpStream.setEncoding("ascii");
	webSocket.on('message', function (message) {
		try
		{
			if(message.split(' ')[0] == 'CONNECT')
			{
				var host = message.split(' ')[1].split(':')[0];
				var port = message.split(' ')[1].split(':')[1];
				console.log( 'connecting to '+host+':'+port+'…' );
				tcpStream.destroy();
				tcpStream.connect( port, host );
			}
			else
			{
				tcpStream.write(message);
			}
		}
		catch (e)
		{
			webSocket.send(e);
		}
	});
	
		webSocket.on('disconnect', function () {
			tcpStream.end();
	});

	tcpStream.addListener("error", function (error){
		webSocket.send(error+"\r\n");
	});
	tcpStream.addListener("data", function (data) {
		webSocket.send(data);
	});
	
	
	tcpStream.addListener("close", function (){
		webSocket.send("O servidor finalizou a conexão e agora você está offline. Use /connect para conectar novamente.");
	});
});




